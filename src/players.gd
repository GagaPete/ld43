extends Node

signal keyboard_joined
signal gamepad_joined

var accept_joins = false
var keyboard
var keyboard_stats
var gamepads
var gamepads_stats
var count

class Stats:
	var towers_built = 0
	var damage_taken = 0
	var kills = 0

func _ready():
	reset()
	Input.connect("joy_connection_changed", self, "_on_input_joy_connection_changed")

func _input(event):
	if not accept_joins:
		return

	if not event.is_action("game_join"):
		return

	if event is InputEventKey:
		if keyboard:
			return

		keyboard = true
		keyboard_stats = Stats.new()
		count += 1
		emit_signal("keyboard_joined")
		return

	if event is InputEventJoypadButton:
		if gamepads.has(event.device):
			return

		gamepads.append(event.device)
		gamepads_stats[event.device] = Stats.new()
		count += 1
		emit_signal("gamepad_joined", event.device)
		return

func _on_input_joy_connection_changed(device, connected):
	if not connected:
		count -= 1
		gamepads.erase(device)

func reset():
	keyboard = false
	keyboard_stats = null
	gamepads = []
	gamepads_stats = {}
	count = 0