extends CanvasLayer

signal hidden

export(bool) var success
onready var stats = $main/container/vbox/stats

func _ready():
	update_stats()

func _input(event):
	if not $main.visible:
		return

	if (event.is_action("game_spawn_tower") or event.is_action("game_join")) and event.is_pressed():
		$main.visible = false
		emit_signal("hidden")

func show():
	$main.visible = true
	update_stats()

func update_stats():
	while stats.get_child_count() > 4:
		stats.remove_child(stats.get_child(4))

	if Players.keyboard_stats != null:
		var player = Players.keyboard_stats
		push_label("Keyboard")
		push_label(player.towers_built)
		push_label(player.damage_taken)
		push_label(player.kills)

	for device in Players.gamepads_stats:
		var player = Players.gamepads_stats[device]
		push_label("Gamepad %d" % device)
		push_label(player.towers_built)
		push_label(player.damage_taken)
		push_label(player.kills)

func push_label(text):
	var label = Label.new()
	label.text = str(text)
	stats.add_child(label)