extends Node2D

var transisting = false
var level = 0
var levels = [
	preload("res://levels/0_start/start.tscn"),
	preload("res://levels/1_garden/garden.tscn"),
	preload("res://levels/2_ice/ice.tscn"),
	preload("res://levels/3_cave/cave.tscn"),
	preload("res://levels/4_vulkan/vulkan.tscn"),
	preload("res://levels/X_credits/credits.tscn"),
];

func _ready():
	randomize()
	start_level(levels[level])

func _input(event):
	if event.is_action("game_skip_level") and event.is_pressed():
		if (level + 1) < levels.size():
			level += 1
			start_level(levels[level])

func start_level(level_scene):
	if transisting:
		return

	transisting = true
	get_tree().paused = true
	if has_node("level"):
		var old_level = $level
		old_level.name = "old_level"
		$tween.interpolate_property($overlay, "modulate", Color(0.0, 0.0, 0.0, 0.0), Color(0.0, 0.0, 0.0, 1.0), 0.5, Tween.TRANS_EXPO, Tween.EASE_IN)
		$tween.start()
		yield($tween, "tween_completed")
		old_level.queue_free()

	var level = level_scene.instance()
	level.connect("completed", self, "_on_level_completed")
	level.connect("failed", self, "_on_level_failed")
	add_child(level)
	move_child(level, 0)

	get_tree().paused = false

	$tween.interpolate_property($overlay, "modulate", Color(0.0, 0.0, 0.0, 1.0), Color(0.0, 0.0, 0.0, 0.0), 0.5, Tween.TRANS_EXPO, Tween.EASE_IN)
	$tween.start()
	yield($tween, "tween_completed")
	transisting = false

func _on_level_completed():
	$completed.play()
	if (level + 1) < levels.size():
		level += 1
		start_level(levels[level])
	else:
		yield(show_gameover(true), "completed")
		level = 0
		randomize()
		start_level(levels[level])

func _on_level_failed():
	yield(show_gameover(false), "completed")
	level = 0
	randomize()
	start_level(levels[level])

func show_gameover(success):
	get_tree().paused = true
	$gameover.success = success
	$gameover.show()
	yield($gameover, "hidden")
	get_tree().paused = false