extends KinematicBody2D

export(float) var speed

var follow_king
var target
var killed = false

func _ready():
	choose_follow_king()

func choose_follow_king():
	var kings = get_tree().get_nodes_in_group("king")
	if kings.size() > 0:
		follow_king = weakref(kings[randi() % kings.size()])

func _physics_process(delta):
	if killed:
		return

	if follow_king == null or follow_king.get_ref() == null:
		choose_follow_king()

	if follow_king == null or follow_king.get_ref() == null:
		return

	var king = follow_king.get_ref()
	var distance = (king.global_position - position)
	move_and_slide((distance.normalized() * speed).clamped(distance.length() / delta))

func _on_attack_range_body_entered(body):
	target = body
	_on_attack_timer_timeout()
	$attack_timer.start()

func _on_attack_range_body_exited(body):
	target = null
	$attack_timer.stop()

func _on_attack_timer_timeout():
	target.hurt(4)
	match randi() % 3:
		0: $scatter1.play()
		1: $scatter2.play()
		2: $scatter3.play()

func kill():
	killed = true
	$sprite/faces/idle.visible = false
	$sprite/faces/hurt.visible = true
	$tween.interpolate_property(self, "scale", scale, scale * 1.5, 0.3, Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
	$tween.interpolate_property(self, "modulate", modulate, Color(1.0, 1.0, 1.0, 0.0), 0.3, Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
	$tween.start()
	yield($tween,"tween_completed")
	queue_free()