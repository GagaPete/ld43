extends Node2D

export(float) var interval
export(int) var amount
export(PackedScene) var scene
export(NodePath) var target
export(bool) var active

var target_node

func _ready():
	target_node = get_node(target)
	$timer.wait_time = interval

func _on_timer_timeout():
	if not active:
		return

	var viewport_size = get_viewport().get_visible_rect().size
	var instance = scene.instance()
	var offset = Vector2(viewport_size.length() / 2 + 100, 0)
	instance.global_position = (viewport_size / 2) + offset.rotated(randf() * 360)
	target_node.add_child(instance)

	amount -= 1
	if amount == 0:
		$timer.stop()