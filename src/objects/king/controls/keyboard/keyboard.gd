extends "res://objects/king/controls/control.gd"

var left_pressed = false
var right_pressed = false
var up_pressed = false
var down_pressed = false

func _input(event):
	if not (event is InputEventKey):
		return
	
	if event.echo:
		return

	if event.is_action("game_spawn_tower") and event.is_pressed():
		emit_signal("spawn_tower")
		return

	if event.is_action("game_left"):
		left_pressed = event.is_pressed()
	if event.is_action("game_right"):
		right_pressed = event.is_pressed()
	if event.is_action("game_up"):
		up_pressed = event.is_pressed()
	if event.is_action("game_down"):
		down_pressed = event.is_pressed()

	direction = Vector2()
	if left_pressed:
		direction.x -= 1.0
	if right_pressed:
		direction.x += 1.0
	if up_pressed:
		direction.y -= 1.0
	if down_pressed:
		direction.y += 1.0