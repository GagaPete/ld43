extends "res://objects/king/controls/control.gd"

export(int) var device

var detected_direction = Vector2()

func _ready():
	get_parent().connect("hurt", self, "_on_king_hurt")

func _input(event):
	if not ((event is InputEventJoypadMotion) or (event is InputEventJoypadButton)):
		return

	if event.device != device:
		return

	if event.is_action("game_spawn_tower") and event.is_pressed():
		emit_signal("spawn_tower")
		return

	if event.is_action("game_left") or event.is_action("game_right"):
		detected_direction.x = event.axis_value

	if event.is_action("game_up") or event.is_action("game_down"):
		detected_direction.y = event.axis_value
	
	if detected_direction.length() > 0.3:
		direction = detected_direction
	else:
		direction = Vector2()

func _on_king_hurt():
	Input.start_joy_vibration(device, 0.0, 0.3, 0.2)
	yield(get_tree().create_timer(0.2), "timeout")
	Input.start_joy_vibration(device, 0.0, 0.15, 0.3)