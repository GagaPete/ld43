extends KinematicBody2D

signal tower_built
signal damage_taken(amount)
signal killed(enemy)
signal hurt

export(PackedScene) var tower

export(float) var max_mass
export(float) var min_speed
export(float) var max_speed

export(float) var min_scale
export(float) var max_scale

var controls
var mass
var speed

func _ready():
	controls = $controls
	controls.connect("spawn_tower", self, "_on_controls_spawn_tower")
	mass = max_mass
	update_speed()
	update_scales()

func _physics_process(delta):
	var movement = move_and_slide(controls.direction.normalized() * speed)

	if movement.x != 0.0 and sign(movement.x) == sign($bodies.scale.x):
		$bodies.scale.x *= -1

	var up = (movement.y < -0.1)
	$bodies/up.visible = up
	$bodies/down.visible = !up

func hurt(damage):
	emit_signal("damage_taken", damage)
	$animations.play("hurt")
	emit_signal("hurt")
	reduce_mass(damage)

func reduce_mass(damage):
	mass -= damage
	if mass < 0.0:
		queue_free()

	update_speed()
	update_scales()

func spawn_tower():
	if mass <= 10:
		return

	match randi() % 3:
		0: $sounds/spawn_tower1.play()
		1: $sounds/spawn_tower2.play()
		2: $sounds/spawn_tower3.play()

	reduce_mass(10)
	var instance = tower.instance()
	instance.connect("killed", self, "_on_tower_killed")
	instance.global_position = $bodies/drop_point.global_position + Vector2(0, 30)
	get_parent().add_child(instance)
	emit_signal("tower_built")

func update_speed():
	speed = lerp(max_speed, min_speed, mass / max_mass)

func update_scales():
	var target_scale = lerp(min_scale, max_scale, (mass / max_mass))
	$tween.interpolate_property(self, "scale", scale, Vector2(target_scale, target_scale), 0.2, Tween.TRANS_BACK, Tween.EASE_OUT)
	$tween.start()

func _on_controls_spawn_tower():
	spawn_tower()

func _on_tower_killed(enemy):
	emit_signal("killed", enemy)