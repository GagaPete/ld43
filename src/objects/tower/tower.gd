extends Node2D

signal killed(enemy)

export(PackedScene) var projectile
export(int) var max_shots

var enemies
var shots
var killed = false
var initial_scale

func _ready():
	enemies = []
	shots = max_shots
	initial_scale = $sprite.scale

func shoot():
	if enemies.size() <= 0 or killed:
		return
	var enemy = enemies[randi() % enemies.size()]
	var angle = (enemy.global_position - global_position).angle()
	var instance = projectile.instance()
	instance.connect("killed", self, "_on_projectile_killed")
	instance.global_position = $sprite/shoot_origin.global_position
	instance.global_rotation = angle
	get_parent().add_child(instance)
	$shoot.play()

	shots -= 1
	var target_scale = initial_scale * Vector2(0.15 + (float(shots) / max_shots) * 0.85, float(shots) / max_shots)
	print(target_scale)
	if shots > 0:
		$tween.interpolate_property($sprite, "scale", $sprite.scale, target_scale, 0.3, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		$tween.start()
	else:
		$tween.interpolate_property($sprite, "scale", $sprite.scale, target_scale, 0.3, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		$tween.interpolate_property(self, "modulate", modulate, Color(1.0, 1.0, 1.0, 0.0), 0.3, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
		$tween.start()
		killed = true
		yield($shoot, "finished")
		queue_free()

func _on_timer_timeout():
	if enemies.size() <= 0:
		$timer.stop()
		return
	shoot()

func _on_range_body_entered(body):
	enemies.append(body)
	if $timer.is_stopped():
		$timer.start()
		shoot()

func _on_range_body_exited(body):
	enemies.erase(body)

func _on_projectile_killed(enemy):
	emit_signal("killed", enemy)
