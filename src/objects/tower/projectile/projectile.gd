extends Area2D

signal killed(enemy)

export(float) var speed

func _physics_process(delta):
	global_position += Vector2(speed, 0).rotated(global_rotation) * delta

func _on_visbility_notifier_screen_exited():
	queue_free()

func _on_projectile_area_entered(area):
	var enemy = area.get_parent()
	if enemy.killed:
		return

	enemy.kill()
	emit_signal("killed", enemy)
