extends Area2D

var obstacles

func _ready():
	obstacles = []

func _on_area_body_entered(body):
	obstacles.append(body)

func _on_area_body_exited(body):
	obstacles.erase(body)
