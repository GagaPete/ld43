extends Node2D

export(PackedScene) var king
export(PackedScene) var keyboard_controls
export(PackedScene) var gamepad_controls
export(NodePath) var target
export(bool) var allow_join

var target_node
var gamepad_players

func _ready():
	target_node = get_node(target)
	Players.connect("keyboard_joined", self, "spawn_keyboard")
	Players.connect("gamepad_joined", self, "spawn_gamepad")
	if Players.keyboard:
		spawn_keyboard()
	for device in Players.gamepads:
		spawn_gamepad(device)

func _enter_tree():
	if allow_join:
		Players.reset()
		Players.accept_joins = true

func _exit_tree():
	if allow_join:
		Players.accept_joins = false

func get_spawn_position():
	var best_positions = []
	var best_count = 9999
	for child in get_children():
		if child.obstacles.size() < best_count:
			best_positions = [child.global_position]
			best_count = child.obstacles.size()
		elif child.obstacles.size() == best_count:
			best_positions.append(child.global_position)
	return best_positions[randi() % best_positions.size()]

func spawn(controls, stats):
	var instance = king.instance()
	instance.connect("damage_taken", self, "_on_king_damage_taken", [stats])
	instance.connect("killed", self, "_on_king_killed", [stats])
	instance.connect("tower_built", self, "_on_king_tower_built", [stats])
	var controls_instance = controls.instance()
	controls_instance.name = "controls"
	instance.global_position = get_spawn_position()
	instance.add_child(controls_instance)
	target_node.add_child(instance)
	return instance

func spawn_keyboard():
	spawn(keyboard_controls, Players.keyboard_stats)

func spawn_gamepad(device):
	var instance = spawn(gamepad_controls, Players.gamepads_stats[device])
	instance.get_node("controls").device = device

func _on_king_tower_built(stats):
	stats.towers_built += 1

func _on_king_damage_taken(amount, stats):
	stats.damage_taken += amount

func _on_king_killed(enemy, stats):
	stats.kills += 1
