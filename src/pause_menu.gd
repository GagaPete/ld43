extends CanvasLayer

func _ready():
	$main.visible = Engine.editor_hint
	if OS.has_feature("web"):
		$main/Container/VBoxContainer/quit.queue_free()
	update_sound_label()
	update_fullscreen_label()

func _input(event):
	if event.is_action("pause") and event.is_pressed():
		if $main.visible:
			hide()
		else:
			show()

func _process(delta):
	update_sound_label()
	update_fullscreen_label()

func show():
	get_tree().paused = true
	$main.visible = true
	$main/Container/VBoxContainer/resume.grab_focus()
	set_process(true)

func hide():
	set_process(false)
	$main.visible = false
	get_tree().paused = false

func _on_sound_toggled(button_pressed):
	var bus_id = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_mute(bus_id, not button_pressed)

func _on_fullscreen_toggled(button_pressed):
	OS.window_fullscreen = button_pressed

func _on_resume_pressed():
	hide()

func _on_quit_pressed():
	get_tree().quit()

func update_sound_label():
	var button = $main/Container/VBoxContainer/GridContainer/sound
	var bus_id = AudioServer.get_bus_index("Master")
	var state = not AudioServer.is_bus_mute(bus_id)
	button.pressed = state
	button.text = "On" if state else "Off"

func update_fullscreen_label():
	var button = $main/Container/VBoxContainer/GridContainer/fullscreen
	var state = OS.window_fullscreen
	button.pressed = state
	button.text = "On" if state else "Off"