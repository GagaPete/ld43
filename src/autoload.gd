extends Node

func _ready():
	if OS.has_feature("web"):
		OS.set_window_maximized(true)

func _input(event):
	if event.is_action("take_screenshot") and event.is_pressed():
		take_screenshot()

func take_screenshot():
	var file = File.new()
	var data = get_viewport().get_texture().get_data()
	data.flip_y()
	var i = 0
	while file.file_exists("user://screenshot%d.png" % i):
		i += 1
	data.save_png("user://screenshot%d.png" % i)
	print("Screenshot saved as screenshot%d.png" %i)