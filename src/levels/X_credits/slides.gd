extends Container

var active = 0
var total

func _ready():
	total = get_child_count() - 2
	get_child(0).visible = true

func _on_timer_timeout():
	var tween = $tween
	tween.interpolate_property(get_child(active), "modulate:a", 1.0, 0.0, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	get_child(active).visible = false
	active += 1
	if active == total:
		$timer.stop()
		get_parent().complete()
		return
	get_child(active).visible = true
	tween.interpolate_property(get_child(active), "modulate:a", 0.0, 1.0, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.start()