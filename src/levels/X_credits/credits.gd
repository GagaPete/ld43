extends Node2D

signal completed
signal failed

var active = 0
var total

func _ready():
	total = $slides.get_child_count()
	$slides.get_child(0).visible = true

func _on_timer_timeout():
	var tween = $tween
	tween.interpolate_property($slides.get_child(active), "modulate:a", 1.0, 0.0, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	$slides.get_child(active).visible = false
	active += 1
	if active == total:
		$timer.stop()
		emit_signal("completed")
		return
	$slides.get_child(active).visible = true
	tween.interpolate_property($slides.get_child(active), "modulate:a", 0.0, 1.0, 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.start()

func update_visible():