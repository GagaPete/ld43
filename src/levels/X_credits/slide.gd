tool
extends CenterContainer

export(String) var what setget set_what
export(String) var who setget set_who

func _process(delta):
	$vbox/what.text = what
	$vbox/who.text = who
	set_process(false)

func set_what(value):
	what = value
	if is_inside_tree():
		$vbox/what.text = value
	else:
		set_process(true)

func set_who(value):
	who = value
	if is_inside_tree():
		$vbox/who.text = value
	else:
		set_process(true)