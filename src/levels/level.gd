extends Node2D

signal completed
signal failed

var total_enemies
var ended = false
var had_players = false

func _ready():
	if Players.count > 0:
		$enemy_spawner.amount *= Players.count
		$enemy_spawner.interval /= Players.count
	total_enemies = $enemy_spawner.amount

	$label_layer/label.text = "{0} / {1}".format([total_enemies, total_enemies])

func _process(delta):
	if not had_players:
		if Players.count > 0:
			had_players = true
		else:
			return

	if ended:
		return

	var alive_kings = get_tree().get_nodes_in_group("king").size()
	if alive_kings == 0:
		emit_signal("failed")
		ended = true

	var alive_enemies = get_tree().get_nodes_in_group("enemy").size()
	var enemies_to_spawn = $enemy_spawner.amount
	var remaining_enemies = (alive_enemies + enemies_to_spawn)
	if remaining_enemies == 0:
		emit_signal("completed")
		ended = true

	$label_layer/label.text = "{0} / {1}".format([remaining_enemies, total_enemies])

# For levels with special winning condition
func complete():
	emit_signal("completed")