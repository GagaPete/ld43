extends Node2D

var seconds_to_launch = 5

func _process(delta):
	if Players.count > 0 and $timer.is_stopped():
		$timer.start()
		update_text()

func update_text():
	if seconds_to_launch > 0:
		$countdown.visible = true
		$countdown.text = "%d" % seconds_to_launch
	else:
		$countdown.text = "Go!"
		$hint.visible = false

func _on_timer_timeout():
	seconds_to_launch -= 1
	update_text()
	if seconds_to_launch == -1:
		$countdown.visible = false
		get_node("../enemy_block").queue_free()