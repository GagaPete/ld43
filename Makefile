archivename=SaltyHorde
itchgame=gagapete/salty-horde

all: publish-all

build-linux:
	mkdir out/linux -p
	cd src/; godot --export "Linux/X11" ../out/linux/$(archivename).x86_64
	rm out/$(archivename)Linux.zip -f
	cd out/linux; zip ../$(archivename)Linux.zip *
	rm out/linux -rf

build-macosx:
	mkdir out/macosx -p
	cd src/; godot --export "Mac OSX" ../out/$(archivename)MacOSX.zip

build-windows:
	mkdir out/windows -p
	cd src/; godot --export "Windows Desktop" ../out/windows/$(archivename).exe
	rm out/$(archivename)Windows.zip -f
	cd out/windows; zip ../$(archivename)Windows.zip *
	rm out/windows -rf

build-html5:
	mkdir out/html5 -p
	cd src/; godot --export "HTML5" ../out/html5/$(archivename).html
	mv out/html5/$(archivename).html out/html5/index.html
	rm out/$(archivename)HTML5.zip -f
	cd out/html5; zip ../$(archivename)HTML5.zip *
	rm out/html5 -rf

build-all: build-linux build-macosx build-windows build-html5

publish-linux: build-linux
	butler push out/$(archivename)Linux.zip $(itchgame):linux

publish-macosx: build-macosx
	butler push out/$(archivename)MacOSX.zip $(itchgame):macosx

publish-windows: build-windows
	butler push out/$(archivename)Windows.zip $(itchgame):windows

publish-html5: build-html5
	butler push out/$(archivename)HTML5.zip $(itchgame):html5

publish-all: publish-linux publish-macosx publish-windows publish-html5
